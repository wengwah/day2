/*

function Parent {

};

function Child {

};

Child.prototype = new Parent();

var child = new Child();

console.log();

*/

console.log("-----Hoisting Variables-------")

var x = 10;


function A() {
  console.log("inside A")
  x = 0;
}

console.log(x);
var a = new A();
console.log(x);


console.log("-----Asynchronous Programming-------")

// Avoid Callback Hell (i.e. do not nest)


